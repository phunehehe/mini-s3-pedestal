FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/mini-s3-pedestal-0.0.1-SNAPSHOT-standalone.jar /mini-s3-pedestal/app.jar

EXPOSE 8080

CMD ["java", "-jar", "/mini-s3-pedestal/app.jar"]
