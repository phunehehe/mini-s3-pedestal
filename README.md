# Mini S3 - Pedestal

[![pipeline status](https://gitlab.com/phunehehe/mini-s3-pedestal/badges/master/pipeline.svg)](https://gitlab.com/phunehehe/mini-s3-pedestal/-/commits/master)
[![coverage report](https://gitlab.com/phunehehe/mini-s3-pedestal/badges/master/coverage.svg)](https://phunehehe.gitlab.io/mini-s3-pedestal/coverage/)

This is a toy implementation of a simple storage service using the [Pedestal
libraries](https://github.com/pedestal/pedestal).

Install dependencies and run tests:

```
$ ./test/test.sh
+ lein cloverage
WARNING: reader-conditional already refers to: #'clojure.core/reader-conditional in namespace: clojure.tools.reader.impl.utils, being replaced by: #'clojure.tools.reader.impl.utils/reader-conditional
WARNING: tagged-literal already refers to: #'clojure.core/tagged-literal in namespace: clojure.tools.reader.impl.utils, being replaced by: #'clojure.tools.reader.impl.utils/tagged-literal
Loading namespaces:  (mini-s3-pedestal.server mini-s3-pedestal.service)
Test namespaces:  (mini-s3-pedestal.service-test)
Loaded  mini-s3-pedestal.service  .
Loaded  mini-s3-pedestal.server  .
Instrumented namespaces.

Testing mini-s3-pedestal.service-test
INFO  io.pedestal.http - {:msg "PUT /files/name2", :line 78}
INFO  io.pedestal.http - {:msg "GET /files/name1", :line 78}
INFO  io.pedestal.http - {:msg "GET /files", :line 78}
INFO  io.pedestal.http - {:msg "DELETE /files/name1", :line 78}

Ran 4 tests containing 8 assertions.
0 failures, 0 errors.
Ran tests.
Writing HTML report to: /home/phunehehe/mini-s3-pedestal/target/coverage/index.html

|--------------------------+---------+---------|
|                Namespace | % Forms | % Lines |
|--------------------------+---------+---------|
|  mini-s3-pedestal.server |   22.22 |   42.86 |
| mini-s3-pedestal.service |   98.88 |  100.00 |
|--------------------------+---------+---------|
|                ALL FILES |   91.84 |   91.67 |
|--------------------------+---------+---------|
+ lein eastwood
== Eastwood 0.2.5 Clojure 1.8.0 JVM 1.8.0_152
Directories scanned for source files:
  src test
== Linting mini-s3-pedestal.service ==
== Linting mini-s3-pedestal.server ==
== Linting mini-s3-pedestal.service-test ==
== Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0
```

Start the development server:

```
$ lein run
INFO  org.eclipse.jetty.util.log - Logging initialized @4677ms to org.eclipse.jetty.util.log.Slf4jLog
INFO  org.eclipse.jetty.server.Server - jetty-9.4.0.v20161208
INFO  o.e.j.server.handler.ContextHandler - Started o.e.j.s.ServletContextHandler@67add4c9{/,null,AVAILABLE}
INFO  o.e.jetty.server.AbstractConnector - Started ServerConnector@43eea3bd{HTTP/1.1,[http/1.1, h2c]}{0.0.0.0:8080}
INFO  org.eclipse.jetty.server.Server - Started @4846ms
```

Upload a file:

```
$ curl --verbose --request PUT --data test localhost:8080/files/my-file
* Hostname was NOT found in DNS cache
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> PUT /files/my-file HTTP/1.1
> User-Agent: curl/7.35.0
> Host: localhost:8080
> Accept: */*
> Content-Length: 4
> Content-Type: application/x-www-form-urlencoded
>
* upload completely sent off: 4 out of 4 bytes
< HTTP/1.1 201 Created
< Date: Thu, 21 Dec 2017 10:51:48 GMT
< Strict-Transport-Security: max-age=31536000; includeSubdomains
< X-Frame-Options: DENY
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< X-Download-Options: noopen
< X-Permitted-Cross-Domain-Policies: none
< Content-Security-Policy: object-src 'none'; script-src 'unsafe-inline' 'unsafe-eval' 'strict-dynamic' https: http:;
< Transfer-Encoding: chunked
<
* Connection #0 to host localhost left intact
```

List all files:

```
$ curl --silent localhost:8080/files | jq
[
  "my-file"
]
```

Download a file:

```
$ curl --verbose localhost:8080/files/my-file
* Hostname was NOT found in DNS cache
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /files/my-file HTTP/1.1
> User-Agent: curl/7.35.0
> Host: localhost:8080
> Accept: */*
>
< HTTP/1.1 200 OK
< Date: Thu, 21 Dec 2017 10:54:20 GMT
< Strict-Transport-Security: max-age=31536000; includeSubdomains
< X-Frame-Options: DENY
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< X-Download-Options: noopen
< X-Permitted-Cross-Domain-Policies: none
< Content-Security-Policy: object-src 'none'; script-src 'unsafe-inline' 'unsafe-eval' 'strict-dynamic' https: http:;
< Content-Type: text/plain
< Transfer-Encoding: chunked
<
* Connection #0 to host localhost left intact
test
```

Delete a file:

```
$ curl --verbose --request DELETE localhost:8080/files/my-file
* Hostname was NOT found in DNS cache
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> DELETE /files/my-file HTTP/1.1
> User-Agent: curl/7.35.0
> Host: localhost:8080
> Accept: */*
>
< HTTP/1.1 204 No Content
< Date: Thu, 21 Dec 2017 10:55:21 GMT
< Strict-Transport-Security: max-age=31536000; includeSubdomains
< X-Frame-Options: DENY
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< X-Download-Options: noopen
< X-Permitted-Cross-Domain-Policies: none
< Content-Security-Policy: object-src 'none'; script-src 'unsafe-inline' 'unsafe-eval' 'strict-dynamic' https: http:;
<
* Connection #0 to host localhost left intact
```
