(defproject mini-s3-pedestal "0.0.1-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Mozilla Public License"
            :url "https://www.mozilla.org/en-US/MPL/2.0/"}
  :dependencies [
                 [ch.qos.logback/logback-classic "1.1.8"]
                 [io.pedestal/pedestal.jetty     "0.5.2"]
                 [org.clojure/data.json          "0.2.6"]
                 ]
  :resource-paths ["config"]
  :profiles {:dev {:aliases {"run-dev" ["trampoline" "run" "-m" "mini-s3-pedestal.server/run-dev"]}
                   :dependencies [[io.pedestal/pedestal.service-tools "0.5.2"]]
                   :plugins [
                             [jonase/eastwood "0.3.6"]
                             [lein-cloverage  "1.0.9"]
                             [lein-kibit      "0.1.6"]
                             ]}}
  :main mini-s3-pedestal.server)
