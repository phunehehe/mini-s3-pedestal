let pkgs = import <nixpkgs> {};
in pkgs.stdenv.mkDerivation {
  name = "mini-s3-pedestal";
  buildInputs = with pkgs; [
    leiningen
  ];
}
