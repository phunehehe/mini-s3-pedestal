(ns mini-s3-pedestal.server
  (:require [io.pedestal.http :as server]
            [mini-s3-pedestal.service :as service]))

(defn -main [& {:keys [args] :or {args {}}}]
  (-> service/service
      (merge args)
      server/create-server
      server/start))

(def run-dev #(-main :args {::server/join? false}))
