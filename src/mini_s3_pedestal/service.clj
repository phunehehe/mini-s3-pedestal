(ns mini-s3-pedestal.service
  (:require
    [clojure.data.json :as json]
    [io.pedestal.http :as http]
    ))

(defn response [status body & {:as headers}]
  {:status status :body body :headers headers})

(defonce db (atom {}))

(def db-interceptor
  {:name :db-interceptor
   :leave (fn [context]
            (if-let [[op & args] (:transaction context)]
              (apply swap! db op args))
            context)})

(def file-put
  {:name :file-put
   :enter (fn [context]
            (let [file-name (get-in context [:request :path-params :file-name])
                  file-content (slurp (get-in context [:request :body]))]
              (assoc context
                     :transaction [assoc file-name file-content]
                     :response (response 201 nil))))})

(def file-list
  {:name :file-list
   :enter (fn [context]
            (assoc context :response (response 200 (json/write-str (keys @db)))))})

(def file-get
  {:name :file-get
   :enter (fn [context]
            (let [file-name (get-in context [:request :path-params :file-name])]
                  (assoc context :response (response 200 (@db file-name)))))})

(def file-delete
  {:name :file-delete
   :enter (fn [context]
            (let [file-name (get-in context [:request :path-params :file-name])]
                  (assoc context
                         :transaction [dissoc file-name]
                         :response (response 204 nil))))})

(def routes #{
              ["/files/:file-name" :put [db-interceptor file-put]]
              ["/files"            :get file-list]
              ["/files/:file-name" :get file-get]
              ["/files/:file-name" :delete [db-interceptor file-delete]]
              })

;; Consumed by mini-s3-pedestal.server/create-server
;; See http/default-interceptors for additional options you can configure
(def service {::http/port 8080
              ::http/routes routes
              ::http/type :jetty})
