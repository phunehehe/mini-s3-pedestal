(ns mini-s3-pedestal.service-test
  (:require
    [clojure.data.json :as json]
    [clojure.test :refer [deftest is use-fixtures]]
    [io.pedestal.http :as bootstrap]
    [io.pedestal.test :refer [response-for]]
    [mini-s3-pedestal.service :as service]
    ))

(def service
  (::bootstrap/service-fn (bootstrap/create-servlet service/service)))

(def test-name "name1")
(def test-content "content1")
(use-fixtures :each (fn [f]
                      (swap! service/db assoc test-name test-content)
                      (f)
                      (swap! service/db empty)))

(deftest file-put-test
  (let [new-name "name2"
        new-content "content2"
        response (response-for service :put (str "/files/" new-name)
                               :body new-content)]
    (is (= 201 (:status response)))
    (is (= new-content (@service/db new-name)))))

(deftest file-list-test
  (let [response (response-for service :get "/files")]
    (is (= 200 (:status response)))
    (is (= [test-name] (json/read-str (:body response))))))

(deftest file-get-test
  (let [response (response-for service :get (str "/files/" test-name))]
    (is (= 200 (:status response)))
    (is (= test-content (:body response)))))

(deftest file-delete-test
  (let [response (response-for service :delete (str "/files/" test-name))]
    (is (= 204 (:status response)))
    (is (= nil (@service/db test-name)))))
